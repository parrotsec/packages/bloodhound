bloodhound (4.3.1-1parrot1) parrot-updates; urgency=medium

  * New upstream version 4.3.1

 -- Nong Hoang Tu <dmknght@parrotsec.org>  Mon, 04 Sep 2023 05:42:54 +0700

bloodhound (4.1.1-0parrot1) parrot-updates; urgency=medium

  * New upstream version 4.1.1

 -- Lorenzo "Palinuro" Faletra <palinuro@parrotsec.org>  Thu, 07 Jul 2022 15:45:56 +0200

bloodhound (4.0.3-0parrot1) lts-updates; urgency=medium

  * Rebuild package for Parrot 5.0.

 -- Lorenzo "Palinuro" Faletra <palinuro@parrotsec.org>  Wed, 13 Oct 2021 14:32:55 +0200

bloodhound (4.0.3-0kali1) kali-dev; urgency=medium

  * kali-ci: disable piuparts
  * Update debian/* files for compilation from source
  * New upstream version 4.0.3

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 16 Jul 2021 10:13:14 +0200

bloodhound (4.0.2+source-0kali2) kali-dev; urgency=medium

  * Remove armhf: electron-packager is not available for armhf
  * Fix typo debian/kali-ci.yml and enable lintian
  * Update lintian-overrides

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 12 Jul 2021 17:34:16 +0200

bloodhound (4.0.2+source-0kali1) kali-dev; urgency=medium

  * Update debian/watch
  * Disable i386 build and lintian in CI
  * New upstream version 4.0.2+source
  * Build from source
  * Add lintian-overrides
  * Update debian/README.Debian

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 25 May 2021 09:30:23 +0200

bloodhound (4.0.2-0kali1) kali-dev; urgency=medium

  [ Kali Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Sophie Brun ]
  * New upstream version 4.0.2
  * Bump Standards-Version to 4.5.1 (no changes)

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 17 Feb 2021 09:35:36 +0100

bloodhound (4.0.1-0kali1) kali-dev; urgency=medium

  * New upstream version 4.0.1

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 30 Nov 2020 11:37:06 +0100

bloodhound (3.0.5-0kali1) kali-dev; urgency=medium

  * New upstream version 3.0.5

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 27 Jul 2020 14:01:43 +0200

bloodhound (3.0.4-0kali1) kali-dev; urgency=medium

  * New upstream version 3.0.4

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 15 Apr 2020 14:05:24 +0200

bloodhound (3.0.3-0kali1) kali-dev; urgency=medium

  * New upstream version 3.0.3

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 17 Mar 2020 10:28:25 +0100

bloodhound (3.0.2-0kali1) kali-dev; urgency=medium

  * New upstream version 3.0.2

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 26 Feb 2020 14:08:47 +0100

bloodhound (3.0.1-0kali1) kali-dev; urgency=medium

  * New upstream version 3.0.1

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 20 Feb 2020 15:54:34 +0100

bloodhound (3.0.0-0kali1) kali-dev; urgency=medium

  [ Raphaël Hertzog ]
  * Add GitLab's CI configuration file
  * Configure git-buildpackage for Kali
  * Update URL in GitLab's CI configuration file

  [ Sophie Brun ]
  * New upstream version 3.0.0

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 17 Feb 2020 14:42:46 +0100

bloodhound (2.2.1-0kali1) kali-dev; urgency=medium

  [ Raphaël Hertzog ]
  * Update Vcs-* fields for the move to gitlab.com

  [ Sophie Brun ]
  * Add debian/gbp.conf
  * Remove i386 and add arm64 (like upstream)
  * Use debhelper-compat 12
  * Bump Standards-Version to 4.4.0
  * Update debian/rules for new debhelper
  * New upstream version 2.2.1
  * Replace symlink to /usr/lib/bloodhound/BloodHound with an helper-script to
    add the --no-sandbox option (see
    https://github.com/BloodHoundAD/BloodHound/issues/259)

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 30 Aug 2019 11:13:07 +0200

bloodhound (2.1.0-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 13 Mar 2019 15:41:42 +0100

bloodhound (2.0.5-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 17 Jan 2019 15:52:26 +0100

bloodhound (2.0.4-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@freexian.com>  Thu, 08 Nov 2018 15:28:58 +0100

bloodhound (2.0.3.1-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@freexian.com>  Mon, 27 Aug 2018 17:13:27 +0200

bloodhound (2.0.3-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@freexian.com>  Tue, 21 Aug 2018 10:45:39 +0200

bloodhound (1.5.2-0kali1) kali-dev; urgency=medium

  * Import new usptream release

 -- Sophie Brun <sophie@freexian.com>  Mon, 16 Apr 2018 11:06:40 +0200

bloodhound (1.5.1-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@freexian.com>  Fri, 06 Apr 2018 10:10:36 +0200

bloodhound (1.4-0kali1) kali-dev; urgency=medium

  * Import new upstream release
  * Fix README.Debian (see bug 4272)

 -- Sophie Brun <sophie@freexian.com>  Fri, 22 Dec 2017 14:23:27 +0100

bloodhound (1.3-0kali2) kali-dev; urgency=medium

  * Drop build for armel

 -- Sophie Brun <sophie@freexian.com>  Thu, 22 Jun 2017 11:57:39 +0200

bloodhound (1.3-0kali1) kali-dev; urgency=medium

  * Initial release (Closes: 3596)

 -- Sophie Brun <sophie@freexian.com>  Thu, 15 Jun 2017 13:16:24 +0200
